﻿namespace Reflect.Infrastructure.Abstraction;

internal interface IReflectionService
{
    void Run();
}

