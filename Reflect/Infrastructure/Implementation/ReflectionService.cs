﻿using Reflect.Core.Implementation;
using Reflect.Infrastructure.Abstraction;
using Reflect.Models;
using System.Diagnostics;
using System.Text.Json;

namespace Reflection.Infrastrusture.Implementation;

internal class ReflectionService : IReflectionService
{
    public void Run()
    {
        int count = 100;
        CSV(count);
        JSON(count);
        Console.ReadKey();
    }

    private void CSV(int count)
    {
        var watch = new Stopwatch();
        watch.Start();
        F before = new F().Get();
        var csvf = new CSVFormatter();
        Console.WriteLine($"CSV");
        for (int i = 0; i < count; i++)
        {
            using (var fs = new FileStream("csv.csv", FileMode.Create))
            {
                csvf.Serialize(fs, before);
            }
        }
        watch.Stop();
        Console.WriteLine($"{count} раз сериализации: {watch.ElapsedMilliseconds} мс");
        watch.Reset();
        watch.Start();
        F after = new F().Get();
        for (int i = 0; i < count; i++)
        {
            using (var fs = new FileStream("csv.csv", FileMode.Open))
            {
                after = (F)csvf.Deserialize(fs);
            }
        }
        watch.Stop();
        Console.WriteLine($"{count} раз десериализации: {watch.ElapsedMilliseconds} мс");
    }
    private void JSON(int count)
    {
        var watch = new Stopwatch();
        watch.Start();
        F before = new F().Get();
        Console.WriteLine($"JSON");
        for (int i = 0; i < count; i++)
        {
            var json = JsonSerializer.Serialize(before);
            File.WriteAllText("json.json", json);
        }
        watch.Stop();
        Console.WriteLine($"{count} раз сериализации: {watch.ElapsedMilliseconds} мс");
        watch.Reset();
        watch.Start();
        F after = new F().Get();
        for (int i = 0; i < count; i++)
        {
            after = JsonSerializer.Deserialize<F>(File.ReadAllText("json.json"));
        }
        watch.Stop();
        Console.WriteLine($"{count} раз десериализации: {watch.ElapsedMilliseconds} мс");
    }
}

